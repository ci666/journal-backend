import uuid
import time
import userdata
from flask import Flask
from flask_restful import Api, Resource, reqparse

class Entries(Resource):
    
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("entryid")
        parser.add_argument("journalid")
        args = parser.parse_args()
        if args["entryid"] != None:
            for entry in entries:
                if (args["entryid"] == entry["entryid"]):
                    return entry, 200
            return args["entryid"], 404
        if args["journalid"] != None:
                for journal in userdata.journals:
                    if journal["journalid"] == args["journalid"]:
                        return journal["entries"], 200
                return "Error: journal id not found", 404
        return "Error: requires journalid or entryid", 404

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("title", required=True, help="title cannot be blank!")
        parser.add_argument("content", required=True, help="content cannot be blank!")
        parser.add_argument("tags", required=True, help="tags cannot be blank!")
        parser.add_argument("journalid", required=True, help="journalid cannot be blank!")
        parser.add_argument("playerid", required=True, help="playerid cannot be blank!")
        args = parser.parse_args()
        
        entry = {
            "title": args["title"],
            "content": args["content"],
            "tags": args["tags"],
            "journalid": args["journalid"],
            "playerid": args["playerid"],
            "entryid": str(uuid.uuid1()),
            "createdtime": time.time(),
            "modifiedtime": None
        }
        entries.append(entry)
        return entry["entryid"], 201

    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument("entryid", required=True, help="entryid cannot be blank!")
        parser.add_argument("title")
        parser.add_argument("content")
        parser.add_argument("tags")
        args = parser.parse_args()

        for entry in entries:
            if (args["entryid"] == entry["entryid"]):
                if args["title"] != None:
                    entry["title"] = args["title"]
                if args["content"] != None:
                    entry["content"] = args["content"]
                if args["tags"] != None:
                    entry["tags"] = args["tags"]
                entry["modifiedtime"] = time.time()
                return entry, 201
        return args["entryid"], 404
   
    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument("entryid", required=True, help="entryid cannot be blank!")
        args = parser.parse_args()
        global entries
        entries = [entry for entry in entries if entry["entryid"] != args["entryid"]]
        return args["entryid"], 200