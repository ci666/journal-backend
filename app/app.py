import os
import redis
import entry
import userdata
from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

#r = redis.from_url(os.environ.get("REDIS_URL"))

api.add_resource(entry.Entries,"/entries/")
app.run(debug=True)