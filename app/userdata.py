users = [
    {
        "name":"john",
        "password":"matrix",
        "playerid":"def96c14-bbbf-11e8-9f9e-acde48001122",
        "campaigns":["2f1aa6dc-bbbf-11e8-a3fe-acde48001122"],
        "journals":["3d518626-bbbd-11e8-98c6-acde48001122"]
    }
]

campaigns = [
    {
        "campaignid":"2f1aa6dc-bbbf-11e8-a3fe-acde48001122",
        "campaignname":"Fall of the Raven Queen",
        "players":["def96c14-bbbf-11e8-9f9e-acde48001122"],
        "journals":["3d518626-bbbd-11e8-98c6-acde48001122"],
        "gm":"def96c14-bbbf-11e8-9f9e-acde48001122"
    }
]

journals = [
    {
        "journalid":"3d518626-bbbd-11e8-98c6-acde48001122",
        "owner":"WAG",
        "campaignid":"2f1aa6dc-bbbf-11e8-a3fe-acde48001122",
        "role":"player",
        "entries":[
            "c7cad4fa-bafd-11e8-b361-acde48001122",
            "c6ac010c-bafd-11e8-beb4-acde48001122",
            "c794a100-bafd-11e8-a266-acde48001122"
            ]
    }
]

entries = [
    {
        "title":"Day1",
        "content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam efficitur nunc dignissim lacinia consectetur. Sed justo est, suscipit quis blandit vitae, condimentum nec arcu. Nam luctus, est ac laoreet tristique, magna ante maximus purus, nec tincidunt augue ligula sed erat. Mauris eros lorem, mollis sed ex vel, gravida varius lacus. In congue fermentum ligula, at ornare orci posuere a. Aliquam rutrum tortor convallis blandit tincidunt. Mauris viverra, lorem id viverra rutrum, nisl eros aliquet felis, a tincidunt ex velit vel ex. Curabitur scelerisque mollis odio. Mauris vitae ipsum id metus aliquam blandit sed non tortor. Ut id rutrum lorem. Etiam id libero nec quam vehicula dapibus.",
        "tags":["#day1","#badidea","#fuck"],
        "journalid":"3d518626-bbbd-11e8-98c6-acde48001122",
        "playerid":"def96c14-bbbf-11e8-9f9e-acde48001122",
        "entryid":"c7cad4fa-bafd-11e8-b361-acde48001122",
        "createdtime":"1537246142.926114",
        "modifiedtime":"1537246144.687853"

    },
    {
        "title":"Day2",
        "content":"Mauris non dapibus nisi, at tincidunt tortor. Mauris ex sem, dapibus non quam sed, dapibus commodo purus. Donec dignissim, libero ut rutrum lacinia, felis sapien aliquet sapien, et placerat purus metus eget sapien. Donec vehicula et urna quis sodales. Nullam tincidunt, risus a euismod interdum, velit magna cursus tellus, ultricies ultrices elit libero vel nisl. In ante lacus, rhoncus nec euismod id, tempor in neque. Fusce laoreet vulputate diam vel egestas. Nam sit amet elit est. Sed ut congue metus, eget cursus lacus. Phasellus mattis quis magna at pharetra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse fermentum at tellus in sodales. Etiam non lacus ut felis iaculis feugiat in eget ex. Integer malesuada sed eros id ultrices. Duis tincidunt tellus non felis posuere pellentesque non nec neque.",
        "tags":["#day2","#goodidea","#fuck"],
        "journalid":"3d518626-bbbd-11e8-98c6-acde48001122",
        "playerid":"def96c14-bbbf-11e8-9f9e-acde48001122",
        "entryid":"c6ac010c-bafd-11e8-beb4-acde48001122",
        "createdtime":"1537246143.961302",
        "modifiedtime":"1537246144.687853"

    },
    {
        "title":"Day3",
        "content":"Duis vehicula semper nisl, vitae aliquet metus ornare sit amet. Integer cursus tellus ac ipsum dapibus, non convallis massa tempor. Donec ut turpis magna. Sed tincidunt fringilla elit nec mollis. Duis et elit eget mauris scelerisque laoreet non nec nibh. Nam imperdiet libero et turpis tempus, in hendrerit dui mattis. Nunc dignissim tortor ipsum, et semper mauris hendrerit at. Quisque sed lacinia metus. Suspendisse potenti. Nulla ante nunc, scelerisque eu faucibus nec, dignissim pulvinar massa. Etiam nec lectus dignissim, tristique nulla vestibulum, convallis neque. Nam fringilla nisl neque, eu laoreet massa commodo eget. Pellentesque non sem vehicula, tincidunt est non, tincidunt urna. Aenean sit amet massa mi. Nullam at aliquam lacus. Etiam vitae sem turpis.",
        "tags":["#day3","#goodidea","#fuck"],
        "journalid":"3d518626-bbbd-11e8-98c6-acde48001122",
        "playerid":"def96c14-bbbf-11e8-9f9e-acde48001122",
        "entryid":"c794a100-bafd-11e8-a266-acde48001122",
        "createdtime":"1537246144.687853",
        "modifiedtime":"1537246144.687853"

    }

]